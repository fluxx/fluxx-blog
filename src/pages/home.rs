use yew::prelude::*;

pub struct Home;
impl Component for Home {
    type Message = ();
    type Properties = ();

    fn create(_props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        unimplemented!()
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="tile is-ancestor is-vertical">
                { self.header() }
                { self.about_me() }
                { self.projects() }
            </div>

        }
    }
}
impl Home {
    fn header(&self) -> Html {
        html!{
            <>
                <section class="hero is-link is-fullheight is-fullheight-with-navbar">
                    <div class="hero-body">
                        <div class="container"> 
                            {"Hello! I am"}
                            <h1 class="title is-1">
                            { "Luka Borković" }
                            </h1>
                            <h2 class="subtitle is-3">
                            { "Sofware engineer" }
                            </h2>
                        </div>
                    </div>
                </section>
            </>
        }
    }

    fn about_me(&self) -> Html {
        html!{
        <section class="section" id="about">
                <div class="section-heading">
                    <h3 class="title is-2">{ "About Me" }</h3>
                    <h4 class="subtitle is-5">{ "Combining many engineering skills to solve any problem given." }</h4>
                        <div class="container">
                            <p>{"I have 10+ years of professional experience in various software and hardware solutions, ranging from embedded hardware design to web development.
                            Over the years, I have utilized various technologies to reach optimal solutions and efficiently solve problems."}</p>
                        </div>
                </div>
                 <div class="columns has-same-height is-gapless">
                        <div class="column">
                            
                            <div class="card">
                            <div class="card-content">
                                <h3 class="title is-4">{"Profile"}</h3>

                                <div class="content">
                                <table class="table-profile">
                                    <tr>
                                    <th colspan="1"></th>
                                    <th colspan="2"></th>
                                    </tr>
                                    <tr>
                                    <td>{"Location:"}</td>
                                    <td>{"Pancevo, Serbia"}</td>
                                    </tr>
                                    <tr>
                                    <td>{"Phone:"}</td>
                                    <td>{"+381-63-213-207"}</td>
                                    </tr>
                                    <tr>
                                    <td>{"Email:"}</td>
                                    <td>{"contact@fluxx.one"}</td>
                                    </tr>
                                </table>
                                </div>
                                <br/>
                                <div class="buttons has-addons is-centered">
                                <a href="https://gitlab.com/fluxx" class="button is-link">{"Gitlab"}</a>
                                <a href="https://www.linkedin.com/in/luka-borkovi%C4%87-8062628a/" class="button is-link">{"LinkedIn"}</a>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="column">
                        
                            <div class="card">
                            <div class="card-image">
                                <figure class="image">
                                <img src="profilna.jpg" alt="Placeholder image"/>
                                </figure>
                            </div>
                            </div>
                        </div>
                        <div class="column">
                       
                            <div class="card">
                            <div class="card-content skills-content">
                                <h3 class="title is-4">{"Languages/Technologies"}</h3>
                                <div class="content">

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"C/C++:"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="90" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"Rust:"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="90" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"Java:"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="75" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"Linux"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="95" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"Hardware design"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="80" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>

                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"CI/CD"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="85" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>
                                <article class="media">
                                    <div class="media-content">
                                    <div class="content">
                                        <p>
                                        <strong>{"Docker"}</strong>
                                        <br/>
                                        <progress class="progress is-primary" value="85" max="100"></progress>
                                        </p>
                                    </div>
                                    </div>
                                </article>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
            </section>
        }
    }

    fn projects(&self) -> Html {
        html! {
            <section class="section" id="services">
                <div class="section-heading">
                <h3 class="title is-2"> {"Projects"}</h3>
                <h4 class="subtitle is-5"> {"Things I've worked on in the past"}</h4>
                </div>
                <div class="container">
                <div class="columns">
                    <div class="column">
                    <div class="box">
                        <div class="content">
                        <h4 class="title is-5">{"Linux embedded development"}</h4>
                        {"I've developed custom services and drivers for Linux embedded devices, ranging from sensor drivers, display drivers etc."}
                        </div>
                    </div>
                    </div>
                    <div class="column">
                    <div class="box">
                        <div class="content">
                        <h4 class="title is-5">{"Backend web development"}</h4>
                        {"Various backend web services written in various technologies (JavaEE, Rust, etc)"}
                        </div>
                    </div>
                    </div>
                </div>

                <div class="columns">
                    <div class="column">
                    <div class="box">
                        <div class="content">
                        <h4 class="title is-5">{"Bare metal hardware/software"}</h4>
                        {"Many application-specific custom hardware projects with firmware."}
                        </div>
                    </div>
                    </div>
                    <div class="column">
                    <div class="box">
                        <div class="content">
                        <h4 class="title is-5">{"Android"}</h4>
                        {"Developed several Android applications, mostly as a frontend to some of the services developed."}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>

        }
    }
}