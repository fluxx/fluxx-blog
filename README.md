# fLuXx-blog

This is a simple Yew blog-like website based on Bulma blog template. It is meant as a personal blog and presentation and I am currently hosting it on my computer.

Here's a link to test it out (if it's down - sorry)  
[fluxx.one](https://fluxx.one)